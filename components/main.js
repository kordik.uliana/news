import React, { useState } from 'react';
import { Button, StyleSheet, Text, View, TouchableOpacity, FlatList, Image, Modal } from 'react-native';
import { gStyle } from '../styles/style';
import { Ionicons } from '@expo/vector-icons';
import Form from './Form';


export default function Main({ navigation }) {
    // const loadScene = () => {
    //     navigation.navigate('Contacts');
    // }

    const [news, setNews] = useState([
        { name: 'google', anons: 'new info', full: 'okeokeokeoek', key: '1', img: 'https://i.pinimg.com/564x/5b/10/b7/5b10b7aaeef2f1e0ab9361e9e05d274a.jpg' },
        { name: 'expo', anons: 'new info', full: 'okeokeokeoek', key: '2', img: 'https://i.pinimg.com/236x/e3/45/c6/e345c66df017074018a75aa911166e96.jpg' },
        { name: 'apple', anons: 'new info', full: 'okeokeokeoek', key: '3', img: 'https://i.pinimg.com/236x/66/a7/05/66a705f95a8e5787d3996d91e33e8ce0.jpg' },
    ]);

    const[modelWindow, setModalWindow]= useState(false);

    const addArticle =( article)=>{
    setNews((list) =>{
        article.key= Math.random().toString();
        return[
            article,
            ...list
        ]
    });
    setModalWindow(false);
    }
    return (
        <View style={gStyle.main}>
            <Modal visible={modelWindow}> 
            <Ionicons name='close' size={34} color="red" style={styles.iconClose} onPress={()=>setModalWindow(false) }/>
            <View >
                <Text style={styles.title}>
                    Modal Window
                </Text>
                <Form addArticle={addArticle}/>
            </View>
            </Modal>
            <Ionicons name='add-circle' size={34} color="black" style={styles.iconAdd}  onPress={()=>setModalWindow(true)}/>
            <Text style={[gStyle.title, styles.header]}>Главная страница</Text>
            {/* <Button title='open' onPress={loadScene} /> */}
            < FlatList data={news} renderItem={({ item }) => (
                <TouchableOpacity style={styles.item} onPress={() => navigation.navigate('FullInfo', item)}>
                    <Image source={{ uri: item.img }}
                        style={{ width: '100%', height: 500 }} />
                    <Text style={styles.title}>{item.name} </Text>
                    <Text style={styles.anons}>{item.anons} </Text>
                </TouchableOpacity>
            )} />
        </View>
    );
}


const styles = StyleSheet.create({
    item: {
        width: '100%',
        marginBottom: 30,

    },
    header: {
        marginBottom: 30
    },
    title: {
        fontFamily: 'mono-bold',
        fontSize: 22,
        textAlign: 'center',
        marginTop: 20,
        color: '#474747'
    },
    anons: {
        fontFamily: 'mono-light',
        fontSize: 16,
        alignContent:'center',
        marginTop: 5,
        color: '#474747'
    },
    iconAdd: {
        textAlign: 'center', marginBottom: 15,
    },
    iconClose:{
        textAlign: 'center', marginBottom: 15, marginTop:17
    }
});