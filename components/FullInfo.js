import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { gStyle } from '../styles/style';



export default function Contacts({ route }) {

    return (
        <View style={gStyle.main}>
            <Image source={{ uri: route.params.img }}
                style={{ width: '100%', height: 500 }} />
            <Text style={[gStyle.title, styles.header]}>{route.params.name} </Text>
            <Text style={styles.full}>{route.params.full} </Text>
        </View>
    );
}


const styles = StyleSheet.create({
full:{
    fontFamily:'mono-light',
    fontSize:16,
    alignContent:'center',
    marginTop:20,
    color:'#f55151'
},
header:{
fontSize:25,
marginTop:25
}
});