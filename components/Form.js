import React from 'react';
import { StyleSheet, Text, View, Image, Button, TextInput } from 'react-native';
import { gStyle } from '../styles/style';
import { Formik } from 'formik';


export default function Form({ addArticle }) {

    return (
        <View>
            <Formik initialValues={{ name: '', anons: '', full: '', img: '' }} onSubmit={(values, action) => {
                addArticle(values);
                action.resetForm(); //очистка формы
            }}>
                {(props) => (
                    <View>
                        <TextInput
                            style={styles.input}
                            value={props.values.name}
                            placeholder='Введите имя'
                            onChangeText={props.handleChange('name')} />

                        <TextInput
                            style={styles.input}
                            value={props.values.anons}
                            multiline
                            placeholder='Введите анонс'
                            onChangeText={props.handleChange('anons')} />

                        <TextInput
                            style={styles.input}
                            value={props.values.full}
                            multiline
                            placeholder='Введите статью'
                            onChangeText={props.handleChange('full')} />

                        <TextInput
                            style={styles.input}
                            value={props.values.img}
                            placeholder='Укажите фото'
                            onChangeText={props.handleChange('img')} />
                        <Button title='Добавить ' onPress={props.handleSubmit} />
                    </View>
                )}
            </Formik>
        </View>
    );
}


const styles = StyleSheet.create({
input:{
    borderWidth:1,
    marginTop:15,
    padding:10,
    marginLeft:5,
    borderColor: 'silver',
    borderRadius:5,
}
});