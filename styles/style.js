import { StyleSheet, Text, View } from 'react-native';

export const gStyle= StyleSheet.create(
    {
        main:{
            flex:1,
            padding: 20,
            backgroundColor:"silver",
            paddingTop:60,
           
        },
        title:{
            textAlign:'center',
            fontSize:20,
            color:'#333',
            fontFamily:'mono-light'

        }
    }
)