
import React, { useState, useEffect  } from 'react';
import { StyleSheet, ActivityIndicator,View  } from 'react-native';
import * as Font from 'expo-font';
import Navigate from './navigate';


const Fonts = () => Font.loadAsync({
  'mono-bold': require('./assets/fonts/IBMPlexMono-Bold.ttf'),
  'mono-light': require('./assets/fonts/IBMPlexMono-Light.ttf'),
});



export default function App() {
  const [font, setFont] = useState(false);

  useEffect(() => {
    // Отложенная загрузка шрифтов
    const loadResources = async () => {
      try {
        await Fonts();
        setFont(true);
      } catch (error) {
        console.error('Error loading fonts:', error);
      }
    };

    loadResources();
  }, []);

  if (!font) {
   
    return (

      <View style={styles.container}>
        <ActivityIndicator  />
      </View>
    );
  }

  // Когда шрифты загружены, отображаем основной контент приложения
  return (
    <Navigate/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});