import Main from './components/main';
import React from 'react';
import FullInfo from './components/FullInfo';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

const Stack = createNativeStackNavigator();

export default function Navigate(){
    return <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen 
            name='Main'
            component = {Main}
            options={
                {
                    title:'Главная ',
                    headerStyle:{
                        backgroundColor:'#eb5d3d',
                        height:110,
                       
                    },
                    headerTitleStyle:{
                        fontWeight:'400',
                        
                    }
                }
            }
            />
              <Stack.Screen 
            name='FullInfo'
            component = {FullInfo}
            options={{title:'Статья '}}
            />
        </Stack.Navigator>
    </NavigationContainer>
}